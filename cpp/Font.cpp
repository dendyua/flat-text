
#include "Font.hpp"

#include <algorithm>

#undef __FTERRORS_H__
#define FT_ERRORDEF( e, v, s )  { e, s },
#define FT_ERROR_START_LIST     {
#define FT_ERROR_END_LIST       { 0, 0 } };

namespace Flat {
namespace Text {

struct {
	int code;
	const char * message;
} static const kFreeTypeErrors[] =
#include FT_ERRORS_H

inline static constexpr int kFreeTypeErrorCount = std::extent_v<decltype(kFreeTypeErrors)>;

}}

#include "Debug.hpp"
#include "Error.hpp"




namespace Flat {
namespace Text {




void checkFontError(const ::FT_Error error)
{
	if (error != ::FT_Err_Ok) {
		const auto what = error <= 0 || error > kFreeTypeErrorCount ? "?" :
				kFreeTypeErrors[error].message;
		throw Error(std::string("FT_Init_FreeType error: ") + what);
	}
}




FontLibrary::FontLibrary() :
	handle([] () -> ::FT_Library {
		::FT_Library h;
		const ::FT_Error error = ::FT_Init_FreeType(&h);
		checkFontError(error);
		return h;
	}())
{
}


FontLibrary::~FontLibrary() noexcept
{
	::FT_Done_FreeType(handle);
}




FontFace::FontFace(const char * const filePath, const FontLibrary & library) :
	handle([&library, filePath] () -> ::FT_Face {
		::FT_Face h;
		const ::FT_Error error = ::FT_New_Face(library.handle, filePath, 0, &h);
		checkFontError(error);
		return h;
	}())
{
}


FontFace::~FontFace() noexcept
{
	::FT_Done_Face(handle);
}




}}
