
#include "TextureGenerator.hpp"

#include <codecvt>
#include <locale>
#include <stdexcept>

#include <freetype/ftbitmap.h>
#include <freetype/ftoutln.h>

#include <msdfgen.h>

#include "Debug.hpp"
#include "Error.hpp"
#include "Font.hpp"
#include "Rectangulator.hpp"




namespace Flat {
namespace Text {




static constexpr int kGlyphMargin = 1;




void TextureGenerator::checkSupported(const Type type, const int channels)
{
	FLAT_CHECK2(type == Type::Float && channels == 3, Error{});
}


struct FtContext {
	msdfgen::Point2 position;
	msdfgen::Shape * shape;
	msdfgen::Contour * contour;
};

static msdfgen::Point2 ftPoint2(const ::FT_Vector & vector)
{
	return msdfgen::Point2(vector.x, vector.y);
}

static int ftMoveTo(const ::FT_Vector * const to, void * const user)
{
	FtContext & context = *reinterpret_cast<FtContext*>(user);
	if (!(context.contour && context.contour->edges.empty())) {
		context.contour = &context.shape->addContour();
	}
	context.position = ftPoint2(*to);
	return 0;
}

static int ftLineTo(const ::FT_Vector * const to, void * const user)
{
	FtContext & context = *reinterpret_cast<FtContext*>(user);
	const msdfgen::Point2 endpoint = ftPoint2(*to);
	if (endpoint != context.position) {
		context.contour->addEdge(new msdfgen::LinearSegment(context.position, endpoint));
		context.position = endpoint;
	}
	return 0;
}

static int ftConicTo(const ::FT_Vector * const control, const ::FT_Vector * const to,
		void * const user)
{
	FtContext & context = *reinterpret_cast<FtContext*>(user);
	context.contour->addEdge(new msdfgen::QuadraticSegment(context.position,
			ftPoint2(*control), ftPoint2(*to)));
	context.position = ftPoint2(*to);
	return 0;
}

static int ftCubicTo(const ::FT_Vector * const control1, const ::FT_Vector * const control2,
		const ::FT_Vector * const to, void * const user)
{
	FtContext & context = *reinterpret_cast<FtContext*>(user);
	context.contour->addEdge(new msdfgen::CubicSegment(context.position,
			ftPoint2(*control1), ftPoint2(*control2), ftPoint2(*to)));
	context.position = ftPoint2(*to);
	return 0;
}


static void loadGlyph(msdfgen::Shape & output, const ::FT_Face face)
{
	FtContext context = {};
	context.shape = &output;
	::FT_Outline_Funcs ftFunctions;
	ftFunctions.move_to = &ftMoveTo;
	ftFunctions.line_to = &ftLineTo;
	ftFunctions.conic_to = &ftConicTo;
	ftFunctions.cubic_to = &ftCubicTo;
	ftFunctions.shift = 0;
	ftFunctions.delta = 0;
	const auto error = ::FT_Outline_Decompose(&face->glyph->outline, &ftFunctions, &context);
	checkFontError(error);

	if (!output.contours.empty() && output.contours.back().edges.empty()) {
		output.contours.pop_back();
	}
}


TextureGenerator::TextureGenerator(const FontLibrary & fontLibrary) :
	fontLibrary_(fontLibrary)
{
}


template <>
TextureGenerator::Result<float, 3> TextureGenerator::generateTyped(const Params & params)
{
	const FontFace face(params.fontFilePath.c_str(), fontLibrary_);

	checkFontError(::FT_Select_Charmap(face.handle, FT_ENCODING_UNICODE));

	const bool hasKerning = (face.handle->face_flags & FT_FACE_FLAG_KERNING) != 0;
	FLAT_DEBUG << "kerning:" << hasKerning;

	const int ascender   = face.handle->ascender;
	const int descender  = face.handle->descender;
	FLAT_DEBUG << "a:" << ascender << "d:" << descender;

	const std::u32string utf32 = [&params] {
		std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter;
		std::u32string s;
		s += converter.from_bytes(params.alphabet);
		return s;
	}();

	typedef Result<float, 3> R;

	struct GlyphRenderInfo {
		bool valid = false;
		msdfgen::Shape shape;
		int translateX, translateY;
		int bitmapX, bitmapY, bitmapWidth, bitmapHeight;
	};

	std::unordered_map<char32_t, GlyphRenderInfo> renderInfos;

	Rectangulator rectangulator(params.textureWidth);

	const auto & ctype = std::use_facet<std::ctype<char>>(std::locale());

	const double scale = double(params.pixelSize) / face.handle->height;

	R result;

	const double vertexScale = 1.0 / ascender;
	result.descent = descender * vertexScale;
	result.range = params.pixelRange / scale * vertexScale;

	for (const char32_t c : utf32) {
		const bool isSpace = c < 128 ? ctype.is(std::ctype_base::space, c) : false;

		const ::FT_UInt index = ::FT_Get_Char_Index(face.handle, c);

		if (isSpace && index == 0) {
			// ignore
			continue;
		}

		FLAT_CHECK2(index != 0, Error{}) << "No index for:" << Debug::Log::hex(c);

		checkFontError(::FT_Load_Glyph(face.handle, index, FT_LOAD_NO_SCALE));

		Glyph glyph;
		glyph.index = index;
		glyph.isSpace = isSpace;

		if (!glyph.isSpace) {
			GlyphRenderInfo render;

			render.shape.contours.clear();
			render.shape.inverseYAxis = false;

			loadGlyph(render.shape, face.handle);

			render.shape.normalize();

			// max. angle
			msdfgen::edgeColoringSimple(render.shape, 3.0);

			const auto bounds = render.shape.getBounds();

			render.translateX = std::lround(bounds.l);
			render.translateY = std::lround(bounds.b);

			const double boundsWidth = bounds.r - bounds.l;
			const double boundsHeight = bounds.t - bounds.b;

			const double targetWidth = boundsWidth * scale;
			const double targetHeight = boundsHeight * scale;

			const int pixelWidth = std::lround(std::ceil(targetWidth));
			const int pixelHeight = std::lround(std::ceil(targetHeight));

			render.bitmapWidth = pixelWidth + (params.pixelRange + kGlyphMargin) * 2;
			render.bitmapHeight = pixelHeight + (params.pixelRange + kGlyphMargin) * 2;

			const auto pos = rectangulator.put(render.bitmapWidth, render.bitmapHeight);

			FLAT_CHECK2(rectangulator.height() <= params.maxTextureHeight, Error{});

			render.bitmapX = pos.x;
			render.bitmapY = pos.y;

			render.valid = true;

			glyph.txx = render.bitmapX + params.pixelRange + kGlyphMargin;
			glyph.txy = render.bitmapY + params.pixelRange + kGlyphMargin;
			glyph.txw = targetWidth;
			glyph.txh = targetHeight;

			glyph.x = bounds.l * vertexScale;
			glyph.y = bounds.b * vertexScale;
			glyph.w = boundsWidth * vertexScale;
			glyph.h = boundsHeight * vertexScale;

			glyph.advanceX = double(face.handle->glyph->advance.x) * vertexScale;
			glyph.advanceY = double(face.handle->glyph->advance.y) * vertexScale;

			renderInfos[c] = render;
		}

		result.glyphs[c] = glyph;
	}

	if (rectangulator.height() == 0) {
		FLAT_DEBUG << "empty texture";
		return {};
	}

	const int fragmentSize = rectangulator.width() * rectangulator.height();

	result.texture.reset(new R::Fragment[fragmentSize]);

	const double range = double(params.pixelRange) / scale * 2.0;
	const msdfgen::Vector2 translate = double(params.pixelRange + kGlyphMargin) / scale;

	for (const char32_t c : utf32) {
		const Glyph & glyph = result.glyphs[c];
		if (glyph.isSpace) continue;

		const GlyphRenderInfo & render = renderInfos[c];
		if (!render.valid) continue;

		msdfgen::Bitmap<float, 3> bitmap(render.bitmapWidth, render.bitmapHeight);

		const auto boundsTranslate = msdfgen::Vector2(render.translateX, render.translateY);

		msdfgen::generateMSDF(bitmap, render.shape, range, scale, translate - boundsTranslate);

		const msdfgen::BitmapConstRef<float, 3> ref = bitmap;

		const R::Fragment * s = reinterpret_cast<const R::Fragment*>(ref.pixels);
		R::Fragment * d = result.texture.get() + render.bitmapY * rectangulator.width() +
				render.bitmapX;

		for (int y = 0; y < ref.height; ++y) {
			std::memcpy(d, s, sizeof(R::Fragment) * ref.width);
			d += rectangulator.width();
			s += ref.width;
		}
	}

	result.textureWidth = rectangulator.width();
	result.textureHeight = rectangulator.height();

	return result;
}




}}
