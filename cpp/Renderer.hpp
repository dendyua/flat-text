
#pragma once

#include <Flat/Vulkan/Vulkan.hpp>

#include "Font.hpp"
#include "TextBlock.hpp"




namespace Flat {
namespace Text {




class Renderer
{
public:
	struct Params {
		::FT_Face face;
	};

	TextBlock createBlock(const Params & params, const std::string_view & string);

	void render(const TextBlock & textBlock, ::VkCommandBuffer commandBuffer);
};




}}
