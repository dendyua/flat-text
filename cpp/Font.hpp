
#pragma once

#include <ft2build.h>
#include <freetype/freetype.h>




namespace Flat {
namespace Text {




FLAT_TEXT_EXPORT extern void checkFontError(::FT_Error error);




class FontLibrary
{
public:
	const ::FT_Library handle;

	FontLibrary();
	~FontLibrary() noexcept;

	FontLibrary(const FontLibrary &) = delete;
	void operator=(const FontLibrary &) = delete;
};




class FontFace
{
public:
	const ::FT_Face handle;

	FontFace(const char * filePath, const FontLibrary & library);
	~FontFace() noexcept;

	FontFace(const FontFace &) = delete;
	void operator=(const FontFace &) = delete;
};




}}
