
#include <fstream>

#include <QCommandLineParser>
#include <QImage>

#include <Flat/Text/Font.hpp>
#include <Flat/Text/TextureGenerator.hpp>

#include "Debug.hpp"




static constexpr int kMaxAlphabetLength = 256 * 4;




int main(int argc, char ** argv)
{
	QCoreApplication app(argc, argv);

	QCommandLineParser parser;

	const QCommandLineOption fontOption("font", "Path to font file", "font");
	parser.addOption(fontOption);

	const QCommandLineOption alphabetOption("alphabet", "Path to alphabet file in UTF-8 encoding",
			"alphabet");
	parser.addOption(alphabetOption);

	parser.process(app);

	const auto fontFilePath = parser.value(fontOption);
	const auto alphabetFilePaths = parser.values(alphabetOption);
	FLAT_DEBUG << fontFilePath << alphabetFilePaths;

	try {
		Flat::Text::FontLibrary fontLibrary;

		Flat::Text::TextureGenerator textureGenerator(fontLibrary);

		Flat::Text::TextureGenerator::Params params;
		params.fontFilePath = fontFilePath.toStdString();
		params.fontSize = 40;
		params.textureWidth = 2048;
		params.maxTextureHeight = 2048;
		params.pixelSize = 80;
		params.pixelRange = 10;

		params.alphabet = [&alphabetFilePaths] () -> std::string {
			std::string a;

			for (const auto & alphabetFilePath : alphabetFilePaths) {
				std::ifstream file(alphabetFilePath.toStdString());
				FLAT_CHECK(file.good()) << "Failed opening:" << alphabetFilePath;

				file.seekg(0, std::ios::end);
				const int64_t size = file.tellg();
				FLAT_CHECK(size <= kMaxAlphabetLength) << "File is too big:" << alphabetFilePath << size;
				file.seekg(0, std::ios::beg);

				std::string s;
				s.resize(size);
				file.read(s.data(), size);
				FLAT_CHECK(!file.fail()) << "Failed reading:" << alphabetFilePath;

				a += s;
			}

			return a;
		}();

		const auto pixelFloatToByte = [] (float x) {
			struct Clamp {
				inline static float clamp(float n, float b) {
					return n >= 0 && n <= b ? n : float(n > 0)*b;
				}
			};
			return uint8_t(Clamp::clamp(256.f*x, 255.f));
		};

		const auto r = textureGenerator.generate<float, 3>(params);

		QImage image(r.textureWidth, r.textureHeight, QImage::Format_RGB32);
		for (int y = 0; y < r.textureHeight; ++y) {
			QRgb * const dl = reinterpret_cast<QRgb*>(image.scanLine(r.textureHeight - 1 - y));
			const Flat::Text::TextureGenerator::Result<float, 3>::Fragment * sl = r.texture.get() + y * r.textureWidth;
			for (int x = 0; x < r.textureWidth; ++x) {
				auto & d = dl[x];
				const auto & s = sl[x];
				d = qRgb(
					pixelFloatToByte(s.v[0]),
					pixelFloatToByte(s.v[1]),
					pixelFloatToByte(s.v[2])
				);
			}
		}
		image.save("out.png", "png");
	} catch (const std::exception & e) {
		FLAT_ERROR << e.what();
		return 1;
	}

	return 0;
}
