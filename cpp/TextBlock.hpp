
#pragma once

#include <cstdint>
#include <vector>




namespace Flat {
namespace Text {




class TextBlock
{
public:

private:
	typedef uint32_t TextureId;

	struct Glyph {

	};

	struct Chunk {
		TextureId textureId;
		std::vector<Glyph> glyphs;
	};

	std::vector<Chunk> chunks_;
};




}}
