
cmake_minimum_required(VERSION 3.3)

set(Flat_Text_MsdfgenPath "" CACHE PATH "")
if (NOT EXISTS "${Flat_Text_MsdfgenPath}/msdfgen.h")
	message(FATAL_ERROR "msdfgen.h not found")
endif()

find_package(Flat REQUIRED Debug)
find_package(Freetype REQUIRED)

function(_flat_text_exec)
	get_filename_component(root_dir "${CMAKE_CURRENT_LIST_DIR}/.." ABSOLUTE)

	flat_add_sources(
		PREFIX lib
		DIR "${root_dir}/cpp"
			Debug.hpp
			Font.cpp
			Renderer.cpp
	)

	add_library(Flat_Text ${flat_library_type} ${lib_SOURCES})

	target_include_directories(Flat_Text INTERFACE "${root_dir}/include" PUBLIC ${FREETYPE_INCLUDE_DIRS})

	set_target_properties(Flat_Text PROPERTIES
		EXCLUDE_FROM_ALL YES
	)

	flat_add_library_export_macro(Flat_Text FLAT_TEXT_EXPORT)

	target_compile_definitions(Flat_Text
		PUBLIC
			"FLAT_TEXT_SOURCE_DIR=\"${root_dir}/cpp\""
	)

	target_link_libraries(Flat_Text
		PUBLIC
			${FREETYPE_LIBRARIES}
			Flat_Debug
			Flat_Vulkan
	)

	# genlib
	flat_add_sources(PREFIX msdfgen SUFFIXES cpp DIR "${Flat_Text_MsdfgenPath}/core"
		Contour
		contour-combiners
		EdgeHolder
		edge-coloring
		edge-segments
		edge-selectors
		equation-solver
		estimate-sdf-error
		msdfgen
		Scanline
		Shape
		SignedDistance
		Vector2
	)

	flat_add_sources(
		PREFIX genlib
		DIR "${root_dir}/cpp"
			Rectangulator.cpp
			TextureGenerator.cpp
	)

	add_library(Flat_Text_Gen ${flat_library_type} ${msdfgen_SOURCES} ${genlib_SOURCES})

	set_target_properties(Flat_Text_Gen PROPERTIES
		EXCLUDE_FROM_ALL YES
	)

	target_include_directories(Flat_Text_Gen PRIVATE "${Flat_Text_MsdfgenPath}")

	target_link_libraries(Flat_Text_Gen PUBLIC Flat_Text)

	# texture-generator
	flat_add_sources(
		PREFIX texgen
		DIR "${root_dir}/tools/texture-generator"
			main.cpp
	)

	add_executable(Flat_Text_TextureGenerator ${texgen_SOURCES})

	set_target_properties(Flat_Text_TextureGenerator PROPERTIES
		EXCLUDE_FROM_ALL YES
	)

	target_compile_definitions(Flat_Text_TextureGenerator
		PUBLIC
			"FLAT_TEXT_TEXTURE_GENERATOR_SOURCE_DIR=\"${root_dir}/tools/texture-generator\""
	)

	target_link_libraries(Flat_Text_TextureGenerator PRIVATE Flat_Debug_Qt Flat_Text_Gen Qt5::Core)
endfunction()

_flat_text_exec()
