
#pragma once




namespace Flat {
namespace Text {




class Rectangulator
{
public:
	struct Pos { int x, y; };

	Rectangulator(int width) noexcept;

	int width() const noexcept { return width_; }
	int height() const noexcept { return height_; }

	Pos put(int width, int height) noexcept;

private:
	const int width_;

	int height_ = 0;

	int currentWidth_ = 0;
	int currentHeight_ = 0;
};




}}
