
#pragma once

#include <array>
#include <memory>
#include <string>
#include <unordered_map>

#include <freetype/fttypes.h>

#include <Flat/Debug/Exception.hpp>




namespace Flat {
namespace Text {




class FontLibrary;




class TextureGenerator
{
public:
	enum class Type {
		Float
	};

	template <typename T> struct type_for;

	struct Params {
		std::string fontFilePath;
		int fontSize;

		int textureWidth;
		int maxTextureHeight;

		int pixelSize;
		int pixelRange;

		std::string alphabet;
	};

	struct Glyph {
		::FT_UInt index;

		bool isSpace;

		double txx, txy, txw, txh;

		double x, y, w, h;
		double advanceX, advanceY;
	};

	template <typename T, int C>
	struct Result {
		int textureWidth;
		int textureHeight;

		struct Fragment {
			std::array<T, C> v;
		};

		std::unique_ptr<Fragment[]> texture;

		std::unordered_map<char32_t, Glyph> glyphs;

		double descent;
		double range;
	};

	TextureGenerator(const FontLibrary & fontLibrary);

	template <typename T, int C>
	Result<T, C> generate(const Params & params);

private:
	template <typename T, int C>
	Result<T, C> generateTyped(const Params & params);

	static void checkSupported(Type type, int channels);

	const FontLibrary & fontLibrary_;
};




template <> struct TextureGenerator::type_for<float> { inline static constexpr Type value = Type::Float; };




template <typename T, int C>
inline TextureGenerator::Result<T, C> TextureGenerator::generate(const Params & params)
{
	checkSupported(type_for<T>::value, C);
	return generateTyped<T, C>(params);
}




}}
