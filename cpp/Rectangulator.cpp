
#include "Rectangulator.hpp"

#include <algorithm>

#include "Debug.hpp"




namespace Flat {
namespace Text {




Rectangulator::Rectangulator(const int width) noexcept :
	width_(width)
{
}


Rectangulator::Pos Rectangulator::put(const int width, const int height) noexcept
{
	FLAT_ASSERT(width > 0);
	FLAT_ASSERT(width <= width_);
	FLAT_ASSERT(height > 0);

	const int nextWidth = currentWidth_ + width;

	if (nextWidth > width_) {
		const auto pos = Pos{0, height_};
		currentHeight_ = height_;
		height_ += height;
		currentWidth_ = width;
		return pos;
	} else {
		const auto pos = Pos{currentWidth_, currentHeight_};
		currentWidth_ += width;
		height_ = std::max(height_, currentHeight_ + height);
		return pos;
	}
}




}}
